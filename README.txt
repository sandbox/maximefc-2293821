CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------
This module adds an option on the vocabulary edit form to allow you to set a
limit of depth for the terms in this vocabulary.

REQUIREMENTS
------------
This module requires the following modules:
* Taxonomy (Core)

INSTALLATION
------------
* Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
The module has no menu or modifiable settings.  There is no
configuration.

MAINTAINERS
-----------
Current maintainers:
* Maxime Gaul (maximefc) - https://drupal.org/user/2770045
This project has been sponsored by:
* FEEL AND CLIC
    www.feelandclic.com
